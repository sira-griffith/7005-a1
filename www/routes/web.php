<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Router Lists
|-------------------------------------------------------------------------- 
*/

/* Pointing to separated route files */
$dir = __DIR__.'/pages/';
$_ROUTER = [
    '/' => $dir.'default.php',
    'home' => $dir.'home.php',
    'booking' => $dir.'booking.php',
    'return' => $dir.'return.php',
    'statistic' => $dir.'statistic.php',
    'vehicle' => $dir.'vehicle.php',
    'client' => $dir.'client.php',
];

/* Default Router */
require($_ROUTER['/']);

/* Home Router */
require($_ROUTER['home']);

/* Booking Router */
require($_ROUTER['booking']);

/* Return Router */
require($_ROUTER['return']);

/* Period Statistic */
require($_ROUTER['statistic']);

/* Vehicle Router*/
require($_ROUTER['vehicle']);

/* Client Router*/
require($_ROUTER['client']);


/* Reset Database */
Route::get('/sys/resetdb', function () {
    $cmd = "cd ../;cd database;rm -f database.sqlite;
        cp database.backup.sqlite database.sqlite;";
    exec($cmd);
    return redirect('/home');
});
