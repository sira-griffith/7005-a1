<?php
/*
|--------------------------------------------------------------------------
|  Client Router
|-------------------------------------------------------------------------- 
*/

/* Client Page */
Route::get('/client', function () {
    $sql = 'SELECT * FROM Client';
    $result = DB::select($sql);
    return view('pages/client')->with("clients", $result);
});

/* Create Client Info */
Route::post('/client/create', function () {
    # Checking all POST must not be empty
    if (
        empty($_POST['licence']) ||
        empty($_POST['fName']) ||
        empty($_POST['lName']) || 
        empty($_POST['phone'])) 
    {
        return redirect('/client');
    }
    # Checking Licence format
    if (
        strlen($_POST['licence']) <> 9 || 
        !is_numeric($_POST['licence'])
    )
    {
        return redirect('/client');
    }
    # Checking Phone format
    if (
        strlen($_POST['phone']) <> 10 || 
        !is_numeric($_POST['phone'])
    )
    {
        return redirect('/client');
    }

    $licence = $_POST['licence'];
    $fName = $_POST['fName'];
    $lName = $_POST['lName'];
    $phone = $_POST['phone'];
    $sql = "INSERT INTO Client (licence, fName, lName, phone) 
        VALUES ('".$licence."', '".$fName."', '".$lName."', '".$phone."')"; 
    DB::insert($sql);
    return redirect('/client');
});

/* Update Client Info */
Route::post('/client/update', function () {
    # Checking all POST must not be empty
    if (
        empty($_POST['idClient']) || 
        empty($_POST['licence']) ||
        empty($_POST['fName']) ||
        empty($_POST['lName']) || 
        empty($_POST['phone'])) 
    {
        return redirect('/client');
    }
    # Checking Licence format
    if (
        strlen($_POST['licence']) <> 9 || 
        !is_numeric($_POST['licence'])
    )
    {
        return redirect('/client');
    }   
    # Checking Phone format
    if (
        strlen($_POST['phone']) <> 10 || 
        !is_numeric($_POST['phone'])
    )
    {
        return redirect('/client');
    }

    $idClient = $_POST['idClient'];
    $licence = $_POST['licence'];
    $fName = $_POST['fName'];
    $lName = $_POST['lName'];
    $phone = $_POST['phone'];
    $sql = "UPDATE Client 
        SET licence='".$licence."', fName='".$fName."', lName='".$lName."', phone='".$phone."'
        WHERE licence = '".$licence."' AND idClient='".$idClient."'";
    DB::update($sql);
    return redirect('/client');
});

/* Delete Client Info */
Route::post('/client/delete', function() {
    $licence = $_POST['licence'];
    $sql = "DELETE FROM Client WHERE licence='".$licence."'";
    DB::delete($sql);
    return redirect('/client');
});