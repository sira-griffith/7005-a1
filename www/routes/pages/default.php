<?php
/*
|--------------------------------------------------------------------------
| Default Router
|-------------------------------------------------------------------------- 
*/

Route::get('/', function () {
    return redirect('/home');
});