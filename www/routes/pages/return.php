<?php
/*
|--------------------------------------------------------------------------
|  Return Router
|-------------------------------------------------------------------------- 
*/


/* Return Vehicle */
Route::post('/return', function () {
    # Checking return POST must not be empty
    if (
        empty($_POST['odometerReturnForm']) || 
        empty($_POST['infoReturnForm'])) 
    {
        return redirect('/booking');
    }

    # Spliting between id of booking and vehicle
    $infoReturnForm = explode("/", $_POST['infoReturnForm']);
    $idBooking = $infoReturnForm[0];
    $idVehicle = $infoReturnForm[1];
    $returnDate = $infoReturnForm[2];
    $odometer = $_POST['odometerReturnForm'];

    # Checking new odometer km must greater than old
    $sqlSelectOdometer = "SELECT odometer FROM Vehicle WHERE idVehicle='".$idVehicle."'";
    $oldOdometer = DB::select($sqlSelectOdometer);
    $oldOdometer = intval($oldOdometer[0]->odometer);
    if ($oldOdometer > $odometer) {
        return redirect('/booking');
    }

    /* If all input are correct, process update data */
    # Delete booking for booking list
    $sqlDeleteBooking = "DELETE FROM Booking WHERE idBooking='".$idBooking."'";
    DB::delete($sqlDeleteBooking);
    # Update driven km into vechicle odometer
    $sqlUpdateOdometer = "UPDATE Vehicle SET odometer='".$odometer."' WHERE idVehicle='".$idVehicle."'";
    DB::delete($sqlUpdateOdometer);

    # Update the number of total booking hour
    // $sqlSelectTotalHour = "SELECT totalHour FROM VehicleBookingLog WHERE idVehicle='".$idVehicle."'";
    // $selectedTotalHour = DB::select($sqlSelectTotalHour);
    // $selectedTotalHour = intval($selectedTotalHour[0]->totalHour) + 1;
    // $sqlUpdateTotalHour = "UPDATE VehicleBookingLog SET totalHour='".$selectedTotalHour ."' WHERE idVehicle='".$idVehicle."'";
    // DB::update($sqlUpdateTotalHour);

    return redirect('/booking');
});
