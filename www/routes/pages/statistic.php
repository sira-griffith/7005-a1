<?php
/*
|--------------------------------------------------------------------------
|  Statistic Router
|-------------------------------------------------------------------------- 
*/

/* Statistic Page */
Route::get('/statistic', function () {
    # Query for getting the number of frequently booking with involved tables
    $sqlFrequency= 'SELECT 
        Vehicle.make || " " || Vehicle.model || " " || Vehicle.year AS vehicleName,
        Vehicle.rego,
        Vehicle.odometer,
        VehicleBookingLog.frequency
    FROM VehicleBookingLog
    INNER JOIN Vehicle ON 
        VehicleBookingLog.idVehicle = Vehicle.idVehicle 
    ORDER BY VehicleBookingLog.frequency DESC';

    # Query for getting the number of total hours booking with involved tables
    $sqlTotalHour = 'SELECT 
        Vehicle.make || " " || Vehicle.model || " " || Vehicle.year AS vehicleName,
        Vehicle.rego,
        Vehicle.odometer,
        VehicleBookingLog.totalHour
    FROM VehicleBookingLog 
    INNER JOIN Vehicle ON 
        VehicleBookingLog.idVehicle = Vehicle.idVehicle 
    ORDER BY VehicleBookingLog.totalHour DESC';

    # Execute query
    $resultFrequency = DB::select($sqlFrequency);
    $resultTotalHour = DB::select($sqlTotalHour);

    return view('pages/statistic', [
        "resultFrequency" => $resultFrequency,
        "resultTotalHour" => $resultTotalHour
    ]);
});
