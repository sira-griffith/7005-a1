<?php
/*
|--------------------------------------------------------------------------
| Booking Router
|-------------------------------------------------------------------------- 
*/

/* Booking Page */
Route::get('/booking', function () {
    # Query for getting client info
    $sqlClient = 'SELECT * FROM Client';
    # Query for getting vehicle info
    $sqlVehicle = 'SELECT idVehicle, rego, make, model, year 
        FROM Vehicle ORDER BY rego';
    # Query for getting booking detail with involved tables
    $sqlBooking = 'SELECT 
        Client.licence, 
        Vehicle.idVehicle,
        Client.fname || " " || Client.lname AS clientName,
        Vehicle.make || " " || Vehicle.model || " " || Vehicle.year AS vehicleName,
        Vehicle.rego,
        Booking.idBooking, Booking.start_date, Booking.return_date
    FROM Booking 
    INNER JOIN Client ON 
        Booking.idClient = Client.idClient
    INNER JOIN Vehicle ON 
        Booking.idVehicle = Vehicle.idVehicle';

    $resultClient = DB::select($sqlClient);
    $resultVehicle = DB::select($sqlVehicle);
    $resultBooking = DB::select($sqlBooking);

    /* Creating future date time for booking and return*/
    $tomorrow = helperFutureDayTime('1');
    $afterTomorrow = helperFutureDayTime('2');
    /* Convert time for suitable format of HTML datetime form*/
    $bookingTime = helperTimeToHTML($tomorrow);
    $returnTime = helperTimeToHTML($afterTomorrow);

    return view('pages/booking', [
        "selectClient" => $resultClient,
        "selectVehicle" => $resultVehicle,
        "booking" => $resultBooking,
        "time" => [
            "today" => $bookingTime,
            "tomorrow" => $returnTime
        ]
    ]);
});

/* Booking Create */
Route::post('/booking/create', function () {
    # Checking all POST must not be empty
    if (
        empty($_POST['idClient']) || 
        empty($_POST['idVehicle']) ||
        empty($_POST['bookingDate']) || 
        empty($_POST['returnDate'])) 
    {
        return redirect('/booking');
    }

    $idClient = $_POST['idClient'];
    $idVehicle = $_POST['idVehicle'];
    $bookingDate = helperTimeToPHP($_POST['bookingDate']);
    $returnDate = helperTimeToPHP($_POST['returnDate']);
    $todayDate = helperCurrentTime();
    
    /* Datetime validation */

    # Checking booking date must be after today date
    $resultBookingAfterToday = helperIsFirstDateBeforeSecond($todayDate, $bookingDate);
    # Checking return date must be after booking date
    $resultReturnAfterBooking = helperIsFirstDateBeforeSecond($bookingDate, $returnDate);
    # Bolth datetime condition must be correct
    if (!($resultBookingAfterToday && $resultReturnAfterBooking)){
        return redirect('/booking');
    }

    # Checking overlap of booking datetime
    # Getting all start and return datetime of booking lists
    $sqlBookingGetDatetime = "SELECT start_date, return_date FROM Booking WHERE idVehicle='".$idVehicle."'";
    $bookingGetDatetime  = DB::select($sqlBookingGetDatetime);
    # Storing start and return datetime in separate array
    $arrBookingStartDate = [];
    $arrBookingReturnDate = [];
    foreach ($bookingGetDatetime as $item) {
        array_push($arrBookingStartDate, $item->start_date);
        array_push($arrBookingReturnDate, $item->return_date);
    }

    /* Checking start booking with exist return date */
    # triger allow to pass codition to make a booking
    $allowedBooking = False;
    # triger variable for > 0 when booking start_date is after all return date in lists
    $isBookingStartDateAfterExistReturnDate = 0;
    foreach ($arrBookingReturnDate as $return_date){
        $result = helperIsFirstDateBeforeSecond($return_date, $bookingDate);
        if ($result){
            $isBookingStartDateAfterExistReturnDate += 1;
        }
    }
    # Checking result all return date must before the date of new booking
    if ($isBookingStartDateAfterExistReturnDate == count($arrBookingReturnDate)){
        $allowedBooking = True;
    }

    /* Checking new return date must before exist booking date */
    if (!$allowedBooking){
        # triger variable for > 0 when booking start_date is after all return date in lists
        $isBookingReturnDateAfterExistStartDate = 0;
        foreach ($arrBookingStartDate as $start_date){
            $result = helperIsFirstDateBeforeSecond($returnDate, $start_date);
            if ($result){
                $isBookingReturnDateAfterExistStartDate += 1;
            }
        }
        # Checking result all start date must after the date of new return date of booking
        if ($isBookingStartDateAfterExistReturnDate == count($arrBookingReturnDate)){
            $allowedBooking = True;
        }
    }
    # Check finial overlap
    if (!$allowedBooking){
        return redirect('/booking');
    }

    $sql = "INSERT INTO Booking (idVehicle, idClient, start_date, return_date) 
        VALUES ('".$idVehicle."', '".$idClient."', '".$bookingDate."', '".$returnDate."')"; 
    DB::insert($sql);
    
    # Update the number of vehicle frequency booking
    $sqlSelectfrequency = "SELECT frequency FROM VehicleBookingLog WHERE idVehicle='".$idVehicle."'";
    $selectedFrequency = DB::select($sqlSelectfrequency);
    $selectedFrequency = intval($selectedFrequency[0]->frequency) + 1;
    $sqlUpdateFrequency  = "UPDATE VehicleBookingLog SET frequency='".$selectedFrequency ."' WHERE idVehicle='".$idVehicle."'";
    DB::update($sqlUpdateFrequency);

    return redirect('/booking');
});
