<?php
/*
|--------------------------------------------------------------------------
| Home Router
|-------------------------------------------------------------------------- 
*/

Route::get('/home', function () {
    $sql = 'SELECT * FROM Vehicle ORDER BY rego';
    $result = DB::select($sql);
    return view('pages/home')->with("vehicles", $result);
});