<?php
/*
|--------------------------------------------------------------------------
|  Vehicle Router
|-------------------------------------------------------------------------- 
*/

/* Vehicle Page */
Route::get('/vehicle', function () {
    $sql = 'SELECT * FROM Vehicle';
    $result = DB::select($sql);
    return view('pages/vehicle')->with("vehicles", $result);
});

/* Update Vehicle Info */
Route::post('/vehicle/update', function () {
    # Checking all POST must not be empty
    if (
        empty($_POST['idVehicle']) || 
        empty($_POST['make']) ||
        empty($_POST['model']) ||
        empty($_POST['year']) || 
        empty($_POST['colour']) || 
        empty($_POST['rego']) || 
        empty($_POST['vin']) || 
        empty($_POST['odometer'])) 
    {
        return redirect('/vehicle');
    }
    # Checking integer type of year, odometer
    if (
        !is_numeric($_POST['year']) ||
        !is_numeric($_POST['odometer'])
    )
    {
        return redirect('/vehicle');
    }
    # Checking REGO length must be at least 6
    if (strlen($_POST['rego']) < 6)
    {
        return redirect('/vehicle');
    }
    # Checking VIN length must be 17 < 99
    if (strlen($_POST['vin']) < 17 || strlen($_POST['vin']) > 99)
    {
        return redirect('/vehicle');
    }

    /* Updating vehicle info */
    $idVehicle = $_POST['idVehicle'];
    $make = $_POST['make'];
    $model = $_POST['model'];
    $year = $_POST['year'];
    $colour = $_POST['colour'];
    $rego = $_POST['rego'];
    $vin = $_POST['vin'];
    $odometer = $_POST['odometer'];
    $sql = "UPDATE Vehicle
        SET make='".$make."', model='".$model."', year='".$year."', colour='".$colour."', 
            rego='".$rego."', vin='".$vin."', odometer='".$odometer."'
            WHERE idVehicle='".$idVehicle."'";
    DB::update($sql);
    return redirect('/vehicle');

});