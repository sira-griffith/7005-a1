<?php

/* Getting current date time */
function helperCurrentTime(){
    $date = new DateTime();
    return $date->format("Y-m-d H:i");
}

/* Getting future date time */
function helperFutureDayTime($increaseNumber){
    $date = new DateTime();
    $date->modify('+'.$increaseNumber.' day');
    return $date->format("Y-m-d H:i"); 
}

/* Convert date time into HTML datetime form */
function helperTimeToHTML($datetime){
    $datetimeHTML = strval($datetime); 
    $datetimeHTML = str_replace(' ', 'T', $datetime);
    return $datetimeHTML;
}

/* Covert date time from HTML to usable format*/
function helperTimeToPHP($datetime){
    $datetime = str_replace('T', ' ', $datetime);
    $datetime = strtotime($datetime);
    $datetime = date("Y-m-d H:i", $datetime);
    return $datetime;
}

/* Covert readable date time into Timestamp format */
function helperTimeToUnixTimestamp($datetime){
    $stm = new DateTime($datetime);
    return $stm->format("U");
}

/* Covert Timestamp format into readable date time*/
function helperUnixTimestampToTime($stm){
    $datetime = new DateTime();
    $datetime->setTimestamp($stm);
    return $datetime->format("Y-m-d H:i");
}

/* Checking first datetime must be before second datetime */
function helperIsFirstDateBeforeSecond($fDatetime, $sDatetime) {
    $fDatetime = new DateTime($fDatetime);
    $sDatetime = new DateTime($sDatetime);
    return $fDatetime < $sDatetime;
}