@extends('layouts.main')
@section('title', 'Booking')
@section('content')

<div class="row">
   <div class="col-md-7">
      @include('components.booking.bookingForm')
   </div>
   <div class="col-md-5">
      @include('components.booking.returnForm')
   </div>
   <div class="col-md-12">
   <br>
   <h5>
      Booking Lists
   </h5>
   <table class="table table-striped">
      <thead>
         <tr>
            <th scope="col">Booking ID</th>
            <th scope="col">Name</th>
            <th scope="col">Licence</th>
            <th scope="col">Vehicle</th>
            <th scope="col">REGO</th>
            <th scope="col">Start</th>
            <th scope="col">Return</th>
         </tr>
      </thead>
      <tbody>
         @forelse($booking as $item)
         <tr>
            <th scope="col">
               {{ $item->idBooking }}
            </th>
            <td>
               {{ $item->clientName }}
            </td>
            <td>
               {{ $item->licence }}
            </td>
            <td>
               {{ $item->vehicleName }}
            </td>
            <td>
               {{ $item->rego }}
            </td>
            <td>
               {{ $item->start_date }}
            </td>
            <td>
               {{ $item->return_date }}
            </td>
        </tr>  
         @empty
        <tr>
            <td>
               <p class="text-danger">
                  <span class="fas fa-exclamation-circle"></span> No Items</p>
            </td>
        </tr>
        @endforelse
      </tbody>
   </table>
   </div>
</div>
@endsection
