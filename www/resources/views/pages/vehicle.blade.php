@extends('layouts.main')
@section('title', 'Vehicle')
@section('content')
<style>
    .form-control {
        min-width: 200px;
    }
</style>
    <h2><span class="fas fa-car"></span> Vehicle</h2>
    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Images</th>
            <th scope="col">Vehicle Info</th>
            <th scope="col">Description</th>
            </tr>
        </thead>
        <tbody>
            @foreach($vehicles as $vehicle)
            <form action="/vehicle/update" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="idVehicle" value="{{ $vehicle->idVehicle }}"> 
                <tr>
                    <th scope="row">{{ $loop->index + 1 }}</th>
                    <td>
                        <img style="width:250px;height:150px;border-radius:5px;" src="{{ asset('images/cars/'.$vehicle->img) }}">
                    </td>
                    <td>
                    <table>
                        <tr>
                            <td><b>Make</b></td>
                            <td>
                                <input class="form-control" name="make" value="{{ $vehicle->make }}">
                            </td>
                        </tr>
                        <tr>
                            <td><b>Model</b></td>
                            <td>
                                <input class="form-control" name="model" value="{{ $vehicle->model }}">
                            </td>
                        </tr>
                        <tr>
                            <td><b>Year</b></td>
                            <td>
                                <input class="form-control" type="number" name="year" value="{{ $vehicle->year }}">
                            </td>
                        </tr>
                        <tr>
                            <td><b>Colour</b></td>
                            <td>
                                <input class="form-control" name="colour" value="{{ $vehicle->colour }}">
                            </td>
                        </tr>
                        <tr>
                            <td><b>REGO</b></td>
                            <td>
                                <input class="form-control" name="rego" value="{{ $vehicle->rego }}">
                            </td>
                        </tr>
                        <tr>
                            <td><b>VIN</b></td>
                            <td>
                                <input class="form-control" name="vin" value="{{ strtoupper($vehicle->vin) }}">
                            </td>
                        </tr>
                        <tr>
                            <td><b>Odometer</b></td>
                            <td>
                                <input class="form-control" type="number" name="odometer" value="{{ $vehicle->odometer }}">
                            </td>
                        </tr>
                    </table>
                    </td>
                    <td>
                        <textarea class="form-control" rows="10" cols="50">
                            {{ $vehicle->detail }}
                        </textarea>
                        <button type="submit" class="btn btn-info" style="margin-top:10px;"
                            <span class="fa fa-save"></span> Save
                        </button>
                    </td>
                </tr>
            </form>
            @endforeach
        </tbody>
        </table>
@endsection