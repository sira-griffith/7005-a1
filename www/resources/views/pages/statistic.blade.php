@extends('layouts.main')
@section('title', 'Statistic')
@section('content')
<div class="row">
   <div class="col-md-6">
      @include('components.statistic.frequently')
   </div>
   <div class="col-md-6">
      @include('components.statistic.totalhour')
   </div>
</div>
@endsection