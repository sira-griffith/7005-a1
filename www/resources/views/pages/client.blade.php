@extends('layouts.main')
@section('title', 'Client')
@section('content')
    <h2><span class="fas fa-users"></span> Client</h2>
    <!-- Create Form -->
    @include('components.client.createForm')
    <!-- Client Lists -->
    <div class="row">
    <div class="col-md-12">
    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Licence</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Phone</th>
            <th scope="col"></th>
            <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
        @forelse($clients as $client)
        <tr>
            <th scope="row">{{ $loop->index + 1 }}</th>
            <form method="post" action="/client/update">
                {{ csrf_field() }}
                <input type="hidden" name="idClient" value="{{ $client->idClient }}">
                <td>
                    <input class="form-control" type="text" name="licence" value="{{ $client->licence }}">
                </td>
                <td>
                    <input class="form-control" type="text" name="fName" value="{{ $client->fName }}">
                </td>
                <td>
                    <input class="form-control" type="text" name="lName" value="{{ $client->lName }}">
                </td>
                <td>
                    <input class="form-control" type="text" name="phone" value="{{ $client->phone }}">
                </td>
                <td>
                    <button type="submit" class="btn btn-info">
                        <span class="fa fa-save"></span>
                    </button>
                </td> 
            </form>
            <td>
            <form method="post" action="/client/delete">
                {{ csrf_field() }}
                <input type="hidden" name="licence" value="{{ $client->licence }}"> 
                <button type="submit" class="btn btn-danger">
                    <span class="fas fa-trash"></span>
                </button>
            </form>
            <td>
        </tr>
        @empty
        <tr>
            <td><p class="text-danger"><span class="fas fa-exclamation-circle"></span> No Items</p></td>
        </tr>
        @endforelse
        </tbody>
        </table>
    </div>
    </div>
@endsection