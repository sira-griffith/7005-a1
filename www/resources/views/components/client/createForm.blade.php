<div class="row">
<form method="post" action="/client/create">
{{ csrf_field() }}
<div class="form-row">
   <div class="form-group col-md-2">
      <label for="licenceForm">Licence No.</label>
      <input id="licenceForm" class="form-control" type="text" name="licence" placeholder="123 456 789">
   </div>
   <div class="form-group col-md-2">
      <label for="phoneForm">Phone</label>
      <input id="phoneForm" class="form-control" type="text" name="phone" placeholder="0412345678">
   </div>
   <div class="form-group col-md-2">
      <label for="fNameForm">First Name</label>
      <input id="fNameForm" class="form-control" type="text" name="fName" placeholder="Foo">
   </div>
   <div class="form-group col-md-2">
      <label for="lNameForm">Last Name</label>
      <input id="lNameForm" class="form-control" type="text" name="lName" placeholder="Bar">
   </div>
   <div class="form-group col-md-2">
      <label for="submitForm">&emsp;&emsp;&emsp;</label>
      &emsp;&emsp;
      <button id="submitForm" type="submit" class="btn btn-success">
         <span class="fas fa-user-plus"></span>&nbsp;Add
      </button>
   </div>
</div>
</form>
</div>
