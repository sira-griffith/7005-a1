<h2>
  <span class="fa fa-hourglass-half"></span> Total Booking Hours
</h2>
<div class="container">
   <div class="row">
      <div class="col-md-12">
         <table class="table table-striped">
            <thead>
               <tr>
                  <th scope="col">Rank</th>
                  <th scope="col">Hour</th>
                  <th scope="col">Vehicle</th>
                  <th scope="col">REGO</th>
                  <th scope="col">Odometer</th>
               </tr>
            </thead>
            <tbody>
               @forelse($resultTotalHour as $item)
               <tr>
                  <th scope="col">
                    {{ $loop->index + 1 }}
                  </th>
                  <td>
                     {{ $item->totalHour }}
                  </td>
                  <td>
                     {{ $item->vehicleName }}
                  </td>
                  <td>
                     {{ $item->rego }}
                  </td>
                  <td>
                     {{ $item->odometer }} Km
                  </td>
               </tr>
               @empty
               <tr>
                  <td>
                     <p class="text-danger">
                        <span class="fas fa-exclamation-circle"></span> No Items
                     </p>
                  </td>
               </tr>
               @endforelse
            </tbody>
         </table>
      </div>
   </div>
</div>