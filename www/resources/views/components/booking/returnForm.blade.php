<h2>
    <span class="fas fa-american-sign-language-interpreting"></span> Return
</h2>
<div>
<form method="post" action="/return">
{{ csrf_field() }}
<div class="form-row" style="max-width:700px">
   <!-- client select form -->
   <div class="form-group col-md-12">
   <label for="infoReturnForm">Booking</label>
      <select class="form-control" 
         id="infoReturnForm" 
         name="infoReturnForm"> 
         @foreach($booking as $item) 
            <option value="{{ $item->idBooking }}/{{ $item->idVehicle }}/{{ $item->return_date }}">
               {{ $item->idBooking }} -
               {{ $item->clientName }} -
               {{ $item->vehicleName }}
            </option>
        @endforeach
      </select>
   </div>
   <!-- Vehicle select form -->
   <div class="form-group col-md-5">
      <label for="odometerReturnForm">Current Odometer</label>
      <input type="number" class="form-control"
         id="odometerReturnForm" name="odometerReturnForm">
   </div>
   <div class="form-group col-md-4">
        <label for="submitReturnForm">
           &emsp;&emsp;&emsp;&emsp;
         </label>
        <button id="submitReturnForm" type="submit" class="btn btn-info" style="float: left;">
            <span class="fas fa-chevron-circle-down"></span>&nbsp;Return
        </button>
    </div>
</div>
</div>
</form>
