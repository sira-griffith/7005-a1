<h2>
  <span class="fas fa-book"></span> Booking
</h2>
<form method="post" action="/booking/create">
  {{ csrf_field() }}
  <div class="form-row" style="max-width:700px">
    <!-- client select form -->
    <div class="form-group col-md-3">
      <label for="clientForm">Booking</label>
      <select class="form-control" id="clientForm" name="idClient"> 
         @foreach($selectClient as $client) 
            <option value="{{ $client->idClient }}">
               {{ $client->fName }}
               {{ $client->lName }}
            </option> 
        @endforeach
      </select>
    </div>
    <!-- Vehicle select form -->
    <div class="form-group col-md-5">
      <label for="clientForm">Vehicle</label>
      <select class="form-control" id="clientForm" name="idVehicle"> 
         @foreach($selectVehicle as $vehicle)
            <option value="{{ $vehicle->idVehicle }}">
               {{ strtoupper($vehicle->rego) }} - {{ $vehicle->make }}
               {{ $vehicle->model }}
               {{ $vehicle->year }}
            </option>
         @endforeach
      </select>
    </div>
  </div>
  <div class="form-row" style="max-width:700px">
    <!-- Booking date form -->
    <div class="form-group col-md-3">
      <label for="bookingForm">Start</label>
      <input type="datetime-local" id="bookingForm" 
         class="form-control" value="{{ $time['today'] }}"
         name="bookingDate" min="{{ $time['today'] }}">
    </div>
    <!-- Return date form -->
    <div class="form-group col-md-3">
      <label for="returnForm">Return</label>
      <input type="datetime-local" id="returnForm" 
         class="form-control" value="{{ $time['tomorrow'] }}"
         name="returnDate" min="{{ $time['tomorrow'] }}">
    </div>
    <!-- Submit form -->
    <div class="form-group col-md-2">
      <label for="submitBookingForm">&emsp;&emsp;&emsp;</label>
      &emsp;&emsp;
      <button id="submitBookingForm" type="submit" class="btn btn-success">
        <span class="fa fa-plus-circle"></span>&nbsp;Book </button>
    </div>
  </div>
</form>